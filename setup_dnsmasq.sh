#!/usr/bin/env bash
#
# Configures dnsmasq for a given domain
# and installs required dependencies
#
# Once configured, wildcards for given domain are resolved
# to localhost without requiring modifications to /etc/hosts
#
# Dependencies:
#  * Xcode Command Line Tools
#  * brew  
#  * dnsmasq
#
# NOTE: This script won't work correctly with *.local domains.
#

if [ $# -ne 1 ]; then
    echo "Usage:" `basename ${0}` "<domain>"
    echo "  <domain> = [app|dev]"
    echo "Example:" `basename ${0}` "app"
    exit 1
fi

XS="/usr/bin/xcode-select"

${XS} -p &> /dev/null
if [ $? -eq 2 ]; then
    echo "Installing Xcode Command Line Tools"

    # to install manually: 
    # xcode-select --install

    # install Xcode Command Line Tools
    # https://github.com/timsutton/osx-vm-templates/blob/ce8df8a7468faa7c5312444ece1b977c1b2f77a4/scripts/xcode-cli-tools.sh
    touch /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress;
    PROD=$(softwareupdate -l |
      grep "\*.*Command Line" |
      head -n 1 | awk -F"*" '{print $2}' |
      sed -e 's/^ *//' |
      tr -d '\n')
    softwareupdate -i "$PROD" -v;
fi

brew --version &> /dev/null
if [ $? -ne 0 ]; then
    echo "Installing brew"
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

brew list dnsmasq &> /dev/null
if [ $? -ne 0 ]; then

    # https://gist.github.com/r10r/5108046
    # ----------------------
    # installing dnsmasq 
    # ----------------------
    brew install dnsmasq
    #sudo cp -v $(brew --prefix dnsmasq)/homebrew.mxcl.dnsmasq.plist /Library/LaunchDaemons
    sudo cp -fv /usr/local/opt/dnsmasq/*.plist /Library/LaunchDaemons
    sudo chown root /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist
fi

# ----------------------
# configure dnsmasq for given domain
# ----------------------
[ -d /usr/local/etc ] || sudo mkdir -p /usr/local/etc
sudo bash -c "echo \"address=/.${1}/127.0.0.1\" > /usr/local/etc/dnsmasq.conf"

# ----------------------
# adding resolver for given domain
# ----------------------
[ -d /etc/resolver ] || sudo mkdir -v /etc/resolver
sudo bash -c "echo \"nameserver 127.0.0.1\" > /etc/resolver/${1}"

# ----------------------
# enable daemon
# ----------------------
sudo launchctl unload /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist
sudo launchctl load /Library/LaunchDaemons/homebrew.mxcl.dnsmasq.plist
echo "Enabling dnsmasq daemon"
sleep 3

ping -c 1 "hello.${1}" > /dev/null
if [ $? -eq 0 ]; then
    echo -e "\nSuccessfully configured dnsmasq for http://*.${1}"
else
    echo -e "\nError configuring dnsmasq for http://*.${1}"
fi

